module internal.endian;
import std.traits;
import ldc.llvmasm;
public import std.system: Endian, endian;

enum isNative (T, Endian E) = T.sizeof <= 1 || endian == E;

pragma(inline, true):
long byteswap(long t) pure nothrow @nogc @trusted
{
    return __asm!long("bswap $0", "=r,r", t);
}

pragma(inline, true):
int byteswap(int t) pure nothrow @nogc @trusted
{
    return __asm!int("bswap $0", "=r,r", t);
}

pragma(inline, true):
short byteswap(short t) pure nothrow @nogc @trusted
{
    return __asm!short(`xchg %al, %ah`, "={ax},{ax}", t);
}

pragma(inline, true):
double byteswap(double t) pure nothrow @nogc @trusted
{
    return __asm!double("bswap $0", "=r,r", t);
}

pragma(inline, true):
float byteswap(float t) pure nothrow @nogc @trusted
{
    return __asm!float("bswap $0", "=r,r", t);
}

unittest
{
    import std.meta;
    import std.stdio;
    foreach (T; AliasSeq!(long, int, short)) {
        scope (failure) writefln("Failed type: %s", T.stringof);
        T val;
        const T cval;
        immutable T ival;

        assert(byteswap(byteswap(val)) == val);
        assert(byteswap(byteswap(cval)) == cval);
        assert(byteswap(byteswap(ival)) == ival);
        assert(byteswap(byteswap(T.min)) == T.min);
        assert(byteswap(byteswap(T.max)) == T.max);

        foreach (i; 2 .. 10) {
            immutable T maxI = cast(T) (T.max / i);
            immutable T minI = cast(T) (T.min / i);

            assert(byteswap(byteswap(maxI)) == maxI);
        }
    }
    assert(byteswap(byteswap(double.min_normal)) == double.min_normal);
    assert(byteswap(byteswap(float.min_normal)) == float.min_normal);
}
