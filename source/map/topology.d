module map.topology;
import io.data;
import std.conv: to;
import std.variant: Algebraic;


struct TopologyMapBase {
    short x;
    short y;
    short z;

    static TopologyMapBase read()(auto ref BinaryDataReader stream) {
        auto x = to!short (stream.read!short * 18),
          y = to!short (stream.read!short * 18),
          z = stream.read!short;

        return TopologyMapBase(x, y, z);
    }
}

struct TopologyMapBlockedCells {
    TopologyMapBase base;
    alias base this;

    byte[] blockedCells;

    static TopologyMapBlockedCells read()(auto ref BinaryDataReader stream) {
        return TopologyMapBlockedCells(
            TopologyMapBase.read(stream),
            stream.readArray!byte (DATA_SIZE)
        );
    }

    bool isCellBlocked(int x, int y)
    {
        auto index = (y - this.y) * 18 + x - this.x,
          unitPosition = index >> 3,
          bitPosition = 7 - (index - (unitPosition << 3));
        return (blockedCells[unitPosition] & (1 << bitPosition)) != 0;
    }

private:
    enum DATA_SIZE = 324 + 7 >> 3;
}

struct TopologyMapA {
    TopologyMapBase base;
    alias base this;

    byte cost;
    byte murfin;
    byte property;

    static TopologyMapA read()(auto ref BinaryDataReader stream) {
        return TopologyMapA(
            TopologyMapBase.read(stream),
            stream.read!byte,
            stream.read!byte,
            stream.read!byte
        );
    }

    bool isCellBlocked(int x, int y) { return cost == -1; }
}

struct TopologyMapB {
    TopologyMapBlockedCells base;
    alias base this;

    byte[] costs;
    byte[] murfins;
    byte[] properties;

    static TopologyMapB read()(auto ref BinaryDataReader stream) {
        auto base = TopologyMapBlockedCells.read(stream),
          costs = new byte[324],
          murfins = new byte[324],
          properties = new byte[324];

        for (int i = 0; i < 324; i++) {
            costs[i] = stream.read!byte;
            murfins[i] = stream.read!byte;
            properties[i] = stream.read!byte;
        }
        return TopologyMapB(base, costs, murfins, properties);
    }
}

struct TopologyMapBi {
    TopologyMapBlockedCells base;
    alias base this;

    byte[] costs;
    byte[] murfins;
    byte[] properties;
    int[] cells;

    static TopologyMapBi read()(auto ref BinaryDataReader stream) {
        auto base = TopologyMapBlockedCells.read(stream),
          indexSize = stream.read!byte,
          costs = new byte[indexSize],
          murfins = new byte[indexSize],
          properties = new byte[indexSize];

        for (int i = 0; i < indexSize; i++) {
            costs[i] = stream.read!byte;
            murfins[i] = stream.read!byte;
            properties[i] = stream.read!byte;
        }
        auto cells = stream.readArray!(ubyte, int);

        return TopologyMapBi(base, costs, murfins, properties, cells);
    }
}

struct TopologyMapC {
    TopologyMapBlockedCells base;
    alias base this;

    byte[] costs;
    byte[] murfins;
    byte[] properties;
    short[] zs;
    byte[] heights;
    byte[] movLos;

    static TopologyMapC read()(auto ref BinaryDataReader stream) {
        auto base = TopologyMapBlockedCells.read(stream),
          costs = new byte[324],
          murfins = new byte[324],
          properties = new byte[324],
          zs = new short[324],
          heights = new byte[324],
          movLos = new byte[324];

        for (int i = 0; i < 324; i++) {
            costs[i] = stream.read!byte;
            murfins[i] = stream.read!byte;
            properties[i] = stream.read!byte;
            zs[i] = stream.read!short;
            heights[i] = stream.read!byte;
            movLos[i] = stream.read!byte;
        }
        return TopologyMapC(base, costs, murfins, properties, zs, heights, movLos);
    }
}

struct TopologyMapCi {
    TopologyMapBlockedCells base;
    alias base this;

    byte[] costs;
    byte[] murfins;
    byte[] properties;
    short[] zs;
    byte[] heights;
    byte[] movLos;
    long[] cells;

    static TopologyMapCi read()(auto ref BinaryDataReader stream) {
        auto base = TopologyMapBlockedCells.read(stream),
          indexSize = stream.read!ubyte,
          costs = new byte[indexSize],
          murfins = new byte[indexSize],
          properties = new byte[indexSize],
          zs = new short[indexSize],
          heights = new byte[indexSize],
          movLos = new byte[indexSize];

        for (int i = 0; i < indexSize; i++) {
            costs[i] = stream.read!byte;
            murfins[i] = stream.read!byte;
            properties[i] = stream.read!byte;
            zs[i] = stream.read!short;
            heights[i] = stream.read!byte;
            movLos[i] = stream.read!byte;
        }
        auto cells = stream.readArray!(ubyte, long);

        return TopologyMapCi(base, costs, murfins, properties, zs, heights, movLos, cells);
    }
}

struct TopologyMapDi {
    TopologyMapBlockedCells base;
    alias base this;

    byte[] costs;
    byte[] murfins;
    byte[] properties;
    short[] zs;
    byte[] heights;
    byte[] movLos;
    long[] cells;
    int[] cellsWithMultiZ;

    static TopologyMapDi read()(auto ref BinaryDataReader stream) {
        auto base = TopologyMapBlockedCells.read(stream),
          indexSize = stream.read!ubyte,
          costs = new byte[indexSize],
          murfins = new byte[indexSize],
          properties = new byte[indexSize],
          zs = new short[indexSize],
          heights = new byte[indexSize],
          movLos = new byte[indexSize];

        for (int i = 0; i < indexSize; i++) {
            costs[i] = stream.read!byte;
            murfins[i] = stream.read!byte;
            properties[i] = stream.read!byte;
            zs[i] = stream.read!short;
            heights[i] = stream.read!byte;
            movLos[i] = stream.read!byte;
        }
        auto cells = stream.readArray!(ubyte, long),
          cellsWithMultiZ = stream.readArray!(ushort, int);

        return TopologyMapDi(base, costs, murfins, properties, zs, heights, movLos, cells, cellsWithMultiZ);
    }
}

alias TopologyMap = Algebraic!(TopologyMapA, TopologyMapB, TopologyMapBi, TopologyMapC, TopologyMapCi, TopologyMapDi);

TopologyMap readTopology()(auto ref BinaryDataReader stream)
{
    immutable header = stream.read!byte;
    switch (header) {
        case 0: return TopologyMap(TopologyMapA.read(stream));
        case 1: return TopologyMap(TopologyMapB.read(stream));
        case 2: return TopologyMap(TopologyMapBi.read(stream));
        case 3: return TopologyMap(TopologyMapC.read(stream));
        case 4: return TopologyMap(TopologyMapCi.read(stream));
        case 5: return TopologyMap(TopologyMapDi.read(stream));
        default:
            assert(false);
    }
}
