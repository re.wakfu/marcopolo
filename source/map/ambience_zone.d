module ambience_zone;
import io.data;

struct AmbienceZone {
    int zoneId;
    string name;
    int musicPlayListId;
    bool useReverb;
    int soundPreset;
    byte zoneTypeId;
    int graphicAmbienceId;
    short entryGfxId;

    static AmbienceZone read()(auto ref BinaryDataReader stream) {
        auto zoneId = stream.read!int,
          name = stream.readString!short,
          musicPlayListId = stream.read!int,
          useReverb = stream.readBoolBit,
          soundPreset = stream.read!int,
          zoneTypeId = stream.read!byte,
          graphicAmbienceId = stream.read!int,
          entryGfxId = stream.read!short;

        return AmbienceZone(zoneId, name, musicPlayListId, useReverb, soundPreset,
                 zoneTypeId, graphicAmbienceId, entryGfxId);
    }
}

AmbienceZone[] readAmbienceBank()(auto ref BinaryDataReader stream) {
    return stream.readArrayWith!(int, AmbienceZone.read);
}
