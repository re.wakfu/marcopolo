module map.world_info;
import io.data;

struct WorldInfo {
    short worldId;
    short paperMapId;
    int bannerTypeId;
    int papermapBgColor;
    int backGroundColor;
    ParallaxInfo[] parallax;
    byte groupType;
    bool isHavenWorld;
    bool isDungeon;
    bool isExterior;
    bool blackOutOfFight;
    bool pveAllowed;
    bool duelAllowed;
    bool pvpAllowed;
    bool wakfuStasis;
    bool dimensionalBagAllowed;
    byte subscriberWorld;
    int bannerColor;
    bool canZoomOutToGlobalMap;
    bool isDisplayTerritory;
    short altitude;
    Territory[] territories;
    AmbienceZone[] ambienceZones;

    static WorldInfo read()(auto ref BinaryDataReader stream) {
        auto worldId = stream.read!short,
          paperMapId = stream.read!short,
          bannerTypeId = stream.read!int,
          papermapBgColor = stream.read!int,
          backGroundColor = stream.read!int,
          parallax = stream.readArrayWith!(byte, ParallaxInfo.read),
          groundType = stream.read!byte,
          isHavenWorld = stream.read!bool,
          isDungeon = stream.read!bool,
          isExterior = stream.read!bool,
          blackOutOfFight = stream.read!bool,
          pveAllowed = stream.read!bool,
          duelAllowed = stream.read!bool,
          pvpAllowed = stream.read!bool,
          wakfuStasis = stream.read!bool,
          dimensionalBagAllowed = stream.read!bool,
          subscriberWorld = stream.read!byte,
          bannerColor = stream.read!int,
          canZoomOutToGlobalMap = stream.read!bool,
          isDisplayTerritory = stream.read!bool,
          altitude = stream.read!short,
          territories = stream.readArrayWith!(ubyte, Territory.read),
          ambienceZones = stream.readArrayWith!(ubyte, AmbienceZone.read);

        return WorldInfo(worldId, paperMapId, bannerTypeId, papermapBgColor, backGroundColor, parallax,
                 groundType, isHavenWorld, isDungeon, isExterior, blackOutOfFight, pveAllowed, duelAllowed,
                 pvpAllowed, wakfuStasis, dimensionalBagAllowed, subscriberWorld, bannerColor,
                 canZoomOutToGlobalMap, isDisplayTerritory, altitude, territories, ambienceZones);
    }
}

struct ParallaxInfo {
    short worldId;
    bool isBackground;
    float moveSpeed;
    float zoom;
    float zoomSpeed;

    static ParallaxInfo read()(auto ref BinaryDataReader stream) {
        immutable
          worldId = stream.read!short,
          isBackground = stream.read!bool,
          moveSpeed = stream.read!float,
          zoom = stream.read!float,
          zoomSpeed = stream.read!float;

        return ParallaxInfo(worldId, isBackground, moveSpeed, zoom, zoomSpeed);
    }
}

struct Territory {
    int id;
    string name;
    int[] partitions;
    short minLevel;
    short maxLevel;

    static Territory read()(auto ref BinaryDataReader stream) {
        auto
          id = stream.read!int,
          name = stream.readString!ushort,
          partitions = stream.readArray!(int, int),
          minLevel = stream.read!short,
          maxLevel = stream.read!short;

        return Territory(id, name, partitions, minLevel, maxLevel);
    }
}

struct AmbienceZone {
    int zoneId;
    bool isWakfuZone;
    bool isPvpAllowed;
    BalanceData resources;
    BalanceData monsters;
    GroundType[] GroundTypes;

    static AmbienceZone read()(auto ref BinaryDataReader stream) {
        auto zoneId = stream.read!int,
          isWakfuZone = stream.read!bool,
          isPvpAllowed = stream.read!bool,
          resources = BalanceData.read(stream),
          monsters = BalanceData.read(stream),
          groundTypes = stream.readArrayWith!(int, GroundType.read);

        return AmbienceZone(zoneId, isWakfuZone, isPvpAllowed, resources, monsters, groundTypes);
    }
}

struct GroundType {
    int a;
    int[] b;

    static GroundType read()(auto ref BinaryDataReader stream) {
        return GroundType(stream.read!int, stream.readArray!(int, int));
    }
}

struct BalanceData {
    ushort maxQuantity;
    ushort stasisThreshold;
    BalanceFamily[] resourceFamilies;

    static BalanceData read()(auto ref BinaryDataReader stream) {
        return BalanceData(stream.read!ushort, stream.read!ushort,
                 stream.readArrayWith!(ubyte, BalanceFamily.read));
    }
}

struct BalanceFamily {
    int id;
    int minQuantity;
    int maxQuantity;

    static BalanceFamily read()(auto ref BinaryDataReader stream) {
        return BalanceFamily(stream.read!int, stream.read!int, stream.read!int);
    }
}

WorldInfo[] readWorldInfos()(auto ref BinaryDataReader stream) {
    return stream.readArrayWith!(short, WorldInfo.read);
}
