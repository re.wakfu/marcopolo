module map.partition;
import io.data;


struct MapCoords {
    int[] coords;

    bool doesPartitionExist(int x, int y)
    {
        import std.algorithm.searching : canFind;
        import util : getIntFromTwoInt;

        return coords.canFind(getIntFromTwoInt(x, y));
    }
}

MapCoords readPartition()(auto ref BinaryDataReader stream)
{
    immutable length = stream.remaining / 4;
    auto coords = new int[length];
    for (int i = 0; i < length; i++) {
        coords[i] = stream.read!int;
    }
    return MapCoords(coords);
}
